﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using HtmlAgilityPack;

namespace NSopSucker
{

    /// <summary>
    /// Methods to extract text from the HTML versions of SOPs, as a precursor to parsing.
    /// </summary>
    static class SopTextExtraction
    {


        public static IEnumerable<string> LoadFRefs(String newLineDelimetedListOfRefs)
        {
            return newLineDelimetedListOfRefs.Split('\n').Select(s => s.Trim(' ', '\n', '\r'));
        }

        public static String GetText(HtmlDocument htmlDocument)
        {
            var sb = new StringBuilder();
            var textNodes = htmlDocument.DocumentNode.SelectNodes("//*[not(self::script or self::style)]/text()");
            textNodes.ToList().ForEach(node => sb.Append(HtmlEntity.DeEntitize(node.InnerText)));
            return sb.ToString();
        }


        public static IEnumerable<string> ExtractParagraphs(string text)
        {
            var r = new Regex(@"^[0-9]+\.\s+.*(?!^[0-9]+\.\s+)", RegexOptions.Singleline);
            var matches = r.Matches(text);
            foreach (var m in matches)
            {
                yield return m.ToString();
            }
        }

        public static string ExtractPara(string text, string paragraphName)
        {

            //var regex = new Regex(@"Factors\n+([0-9]+)\..*(?=[0-9]+\.\s+)", RegexOptions.Singleline);
            // the only thing we want to assume is that the factors para is entitled 'Factors'

            var paraHeadingRegex = new Regex($"^{paragraphName}$");
            var nextHeaderLine = new Regex(@"^([0-9]+)\.\s+");
            List<string> paraLines = new List<string>();
            bool passedPara = false;
            bool finishedPara = false;
            int? paraNumber = null;
            List<String> lines = text.Split('\n').ToList();
            for (int i = 0; i < lines.Count; i++)
            {
                var currentLine = lines.ElementAt(i);
                if (!passedPara && paraHeadingRegex.IsMatch(currentLine))
                    passedPara = true;

                if (passedPara && paraNumber.HasValue && !finishedPara && nextHeaderLine.IsMatch(currentLine))
                    finishedPara = true;

                if (passedPara && !paraNumber.HasValue && nextHeaderLine.IsMatch(currentLine))
                    paraNumber = Convert.ToInt32(nextHeaderLine.Match(currentLine).Groups[1].Value);


                if (passedPara && !finishedPara)
                    paraLines.Add(currentLine);
            }

            paraLines.RemoveRange(paraLines.Count - 3, 3);

            return Scrub(String.Join("\n", paraLines));

        }




        private static string Scrub(string text)
        {
            var doubleNewLineRegex = new Regex(@"\n(?!\n)");
            var withSingleNewLinesRemoved = doubleNewLineRegex.Replace(text, " ");
            var withDoubleSpacesCompressed = Regex.Replace(withSingleNewLinesRemoved, @"\s{2,}", " ");
            return withDoubleSpacesCompressed.Trim();
        }
    }
}
