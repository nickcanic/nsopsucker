﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NDesk.Options;

namespace NSopSucker
{
    class Program
    {
        static void Main(string[] args)
        {

            try
            {
                string pathToIdsList = null;
                var p = new OptionSet().Add("idList=",
                    "Path to new line delimited file of SOP ids on legislation.gov.au.", v => pathToIdsList = v);

                try
                {
                    p.Parse(args);
                }

                catch (OptionException e)
                {
                    p.WriteOptionDescriptions(Console.Out);
                }

                List<string> ids = File.ReadAllLines(pathToIdsList).ToList();

                DirectoryInfo outputDir = new DirectoryInfo(@"C:\Users\nick_000\Desktop\ScrapedSops");
                foreach (var id in ids)
                {
                    var htmlDoc = WebScraper.FetchSopHtmlDocument(id);
                    Thread.Sleep(100);
                    if (htmlDoc != null)
                    {
                        Console.WriteLine(htmlDoc.DocumentNode.SelectSingleNode("//head/title").InnerText);
                        htmlDoc.Save(Path.Combine(outputDir.FullName, $"{id}.html"));
                    }
                }

            }
            catch (Exception e)
            {
                Environment.ExitCode = -1;
                Console.WriteLine(e);
                return;
            }

        }
    }
}
