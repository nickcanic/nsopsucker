﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace NSopSucker.Model
{
    internal class Section
    {
        public SectionHeading SectionHeading { get; }
        public string Head { get; }
        public IEnumerable<Para> Paragraphs { get; }

        public Section(SectionHeading sectionHeading, string head, IEnumerable<Para> paragraphs)
        {
            SectionHeading = sectionHeading;
            Head = head;
            Paragraphs = paragraphs;
        }

        public StandardOfProof GetStandardOfProof()
        {
            if (Regex.IsMatch(Head, @"(?i)balance of probabilities"))
                return StandardOfProof.BoP;
            if (Regex.IsMatch(Head, @"(?i)reasonable hypothesis"))
                return StandardOfProof.RH;
            throw new SopSuckerException($"Cannot determine standard of proof from this text: {Head}");
        }

    }


}