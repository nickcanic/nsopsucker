using System;

namespace NSopSucker.Model
{
    internal class SubPara
    {
        public string Reference { get; }
        public String Text { get; }

        public SubPara(string reference, string text)
        {
            Reference = reference;
            Text = text;
        }

        public override string ToString()
        {
            return $"Reference: {Reference}, Text: {Text}";
        }
    }
}