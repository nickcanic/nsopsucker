﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSopSucker.Model
{
    class SectionHeading
    {
        public int Number { get; }
        public String Text { get; }

        public SectionHeading(int number, string text)
        {
            Number = number;
            Text = text;
        }
    }
}
