﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSopSucker.Model
{
    class Definition
    {
        public string DefinedWordOrPhrase { get; }
        public string Meaning { get; }

        public Definition(string definedWordOrPhrase, string definition)
        {
            DefinedWordOrPhrase = definedWordOrPhrase;
            Meaning = definition;
        }
    }
}
