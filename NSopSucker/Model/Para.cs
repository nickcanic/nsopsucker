﻿using System.Collections.Generic;
using System.Linq;

namespace NSopSucker.Model
{
    internal class Para
    {
        public IEnumerable<SubPara> SubParas { get; }

        public override string ToString()
        {
            return $"Reference: {Reference}, SubParas: {SubParas.Count()}, Text: {Text}";
        }

        public string Reference { get; }
        public string Text { get; }

        public Para(string reference, string text, IEnumerable<SubPara> subParas = null)
        {
            SubParas = subParas ?? new List<SubPara>();
            Reference = reference;
            Text = text;
        }
    }
}