﻿namespace NSopSucker
{
    class SopInstrument
    {
        public string InstrumentName { get; }
        public string InstrumentId { get; }

        public SopInstrument(string instrumentId, string instrumentName)
        {
            InstrumentName = instrumentName;
            InstrumentId = instrumentId;
        }
    }
}