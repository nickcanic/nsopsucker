﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace NSopSucker
{
    internal static class WebScraper
    {
        public static HtmlDocument FetchSopHtmlDocument(string id)
        {


            var req = CreateWebRequest(id);
            System.Diagnostics.Trace.WriteLine($"Sending request for: {id}...");
            Stopwatch sw = new Stopwatch();
            sw.Start();
            try
            {
                var resp = (HttpWebResponse)req.GetResponse();
                sw.Stop();
                System.Diagnostics.Trace.WriteLine($"Received response in {sw.ElapsedMilliseconds} ms.");
                var respStream = resp.GetResponseStream();
                HtmlDocument htmlDocument = new HtmlDocument();
                htmlDocument.Load(respStream);
                return htmlDocument;
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine($"Exception for id:  {id}: {e.Message}");
                return null;
            }


        }

        private static HttpWebRequest CreateWebRequest(string instrumentId)
        {
            var baseUri = new Uri(Properties.Settings.Default.legislationWebsiteBaseUri);
            var instrumentUri = new Uri(baseUri, $"Latest/{instrumentId}");
            var req = HttpWebRequest.Create(instrumentUri);
            req.Timeout = 10000;
            return (HttpWebRequest)req;
        }


    }
}
