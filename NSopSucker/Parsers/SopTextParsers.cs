﻿using System;
using System.Linq;
using NSopSucker.Model;
using Sprache;

namespace NSopSucker.Parsers
{
    internal static class SopTextParsers
    {
        public static Parser<SectionHeading> sectionHeading = (from sectionHeading in Parse.Regex(@"^[A-Z][A-Za-z,\s]+").Token()
                                                               from number in Parse.Number
                                                               let num = Convert.ToInt32(number)
                                                               from dot in Parse.Char('.')
                                                               select new SectionHeading(num, sectionHeading.Trim())).Token();

        public static Parser<string> bodyText = Parse.Regex(@"[A-Za-z0-9\-'’,\)\(\s]+");

        public static Parser<string> head = (from text in bodyText from colon in Parse.Char(':') select text).Token();

        public static Parser<string> paraTerminator = Parse.Regex(@"(;\sor\s)").Or(Parse.Regex(@"\.").Or(Parse.Regex(";")));

        public static Parser<string> tail = from text in bodyText
                                            from close in paraTerminator
                                            select text;

        public static Parser<string> paraRef = (from openParen in Parse.Char('(')
                                                from letters in Parse.Regex(@"[a-z]+")
                                                from closeParen in Parse.Char(')')
                                                from ws in Parse.Regex(@"\s+")
                                                select letters).Token();

        public static Parser<string> subParaRef = (from openParen in Parse.Char('(')
                                                   from letters in Parse.Regex(@"[ivx]+")
                                                   from closeParen in Parse.Char(')')
                                                   from ws in Parse.Regex(@"\s+")
                                                   select letters).Token();


        public static Parser<SubPara> subPara = (from r in subParaRef
                                                 from body in bodyText
                                                 from term in paraTerminator
                                                 select new SubPara(r, body)).Token();


        public static Parser<Para> simplePara = (from reference in paraRef
                                                 from text in bodyText
                                                 from term in paraTerminator
                                                 select new Para(reference, text)).Token();



        public static Parser<Para> paraWithSubParas = (from r in paraRef
                                                       from h in head
                                                       from subparas in subPara.AtLeastOnce()
                                                       from t in tail.Optional()
                                                       let text = $"{h}...[{subparas.Count()} sub-paras]...{t.GetOrDefault()}"
                                                       select new Para(r, text, subparas)).Token();

        public static Parser<Para> para = simplePara.Or(paraWithSubParas);


        public static Parser<Section> section =
            (from heading in sectionHeading
             from h in head
             from paras in para.AtLeastOnce()
             select new Section(heading, h, paras)).Token();


        public static Parser<string> definedTerm = (from openQuote in Parse.Char('"')
                                                    from definedTerm in Parse.Regex(@"[A-Za-z0-9\s\-]+")
                                                    from closeQuote in Parse.Char('"')
                                                    select definedTerm).Token();

        public static Parser<Definition> definition =
            (from term in definedTerm from meaning in Parse.Regex(@"[^""]+") select new Definition(term, meaning.Trim().TrimEnd(';', '.'))).Token();






    }


}