﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NSopSucker.Tests.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("NSopSucker.Tests.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Factors 6. The factor that must as a minimum exist before it can be said that a reasonable hypothesis has been raised connecting lumbar spondylosis or death from lumbar spondylosis with the circumstances of a person’s relevant service is: (a) being a prisoner of war before the clinical onset of lumbar spondylosis; or (b) having inflammatory joint disease in the lumbar spine before the clinical onset of lumbar spondylosis; or (c) having an infection of the affected joint as specified at least one year before [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string FactorsPara {
            get {
                return ResourceManager.GetString("FactorsPara", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to F2015L00920
        ///F2015L00919
        ///F2015L00645
        ///F2015L00648
        ///F2015L00914
        ///F2015L00913
        ///F2015L00254
        ///F2015L00253
        ///F2015L00917
        ///F2015L00918
        ///F2015L00925
        ///F2015L00924
        ///F2015L00908
        ///F2015L00907
        ///F2015L00258
        ///F2015L00257
        ///F2016C00273
        ///F2016C00271
        ///F2015L00654
        ///F2015L00260
        ///F2015L00649
        ///F2015L01671
        ///F2015L00655
        ///F2015L00259
        ///F2015L00909
        ///F2016C00277
        ///F2015L00262
        ///F2016L01144
        ///F2015L00250
        ///F2015L00652
        ///F2016C00278
        ///F2015L00255
        ///F2016C00275
        ///F2016L01356
        ///F2015L00658
        ///F2015C00801
        ///F2015L00910
        ///F2015L00911
        ///F2015C00802
        ///F2015 [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string fRefs {
            get {
                return ResourceManager.GetString("fRefs", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 
        ///
        ///
        ///    
        ///    
        ///    
        ///    
        ///    
        ///	Statement of Principles concerning lumbar spondylosis No. 62 of 2014
        ///
        ///
        ///    Federal Register of Legislation - Australian Government
        ///    
        ///        Skip to primary navigation
        ///        Skip to primary content
        ///    
        ///        
        ///    
        ///    
        ///
        ///
        ///
        ///
        ///
        ///
        ///
        ///
        ///
        ///
        ///
        ///
        ///
        ///
        ///
        ///
        ///
        ///
        ///
        ///
        ///
        ///
        ///
        ///
        ///	
        ///	
        ///
        ///        
        ///
        ///        
        ///        
        ///
        ///
        ///
        ///        
        ///            
        ///
        ///
        ///                
        ///                    
        ///                        
        ///                            
        ///                            
        ///                                
        ///                   [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string lumbarSpondylosisText {
            get {
                return ResourceManager.GetString("lumbarSpondylosisText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;!doctype html&gt;
        ///&lt;!--[if lt IE 7 ]&gt;&lt;html class=&quot;ie ie6 no-js&quot; lang=&quot;en-GB&quot;&gt; &lt;![endif]--&gt;
        ///&lt;!--[if IE 7 ]&gt;&lt;html class=&quot;ie ie7 no-js&quot; lang=&quot;en-GB&quot;&gt; &lt;![endif]--&gt;
        ///&lt;!--[if IE 8 ]&gt;&lt;html class=&quot;ie ie8 no-js&quot; lang=&quot;en-GB&quot;&gt; &lt;![endif]--&gt;
        ///&lt;!--[if IE 9 ]&gt;&lt;html class=&quot;ie ie9 no-js&quot; lang=&quot;en-GB&quot;&gt; &lt;![endif]--&gt;
        ///&lt;!--[if (gte IE 9)|!(IE)]&gt;&lt;!--&gt;&lt;html class=&quot;no-js&quot; lang=&quot;en-GB&quot;  xmlns:og=&quot;http://ogp.me/ns#&quot; xmlns:fb=&quot;http://www.facebook.com/2008/fbml&quot; xmlns:website=&quot;http://ogp.me/ns/website#&quot;&gt; &lt;!--&lt;![endif]--&gt;
        /// &lt;head&gt;
        ///  &lt; [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string Musculoskeletal_System___Repatriation_Medical_Authority {
            get {
                return ResourceManager.GetString("Musculoskeletal_System___Repatriation_Medical_Authority", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Other definitions 9. For the purposes of this Statement of Principles: &quot;a depositional joint disease&quot; means gout, calcium pyrophosphate dihydrate deposition disease (also known as pseudogout), haemochromatosis, Wilson’s disease or alkaptonuria (also known as ochronosis); &quot;a kyphotic abnormality&quot; means abnormally increased dorsal convexity in the curvature of the lumbar vertebral column; &quot;a lordotic abnormality&quot; means abnormally increased dorsal concavity in the curvature of the lumbar vertebral column; &quot;a s [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string rotatorCuffDefinitionsPara {
            get {
                return ResourceManager.GetString("rotatorCuffDefinitionsPara", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Factors 6. The factor that must exist before it can be said that, on the balance of probabilities, rotator cuff syndrome or death from rotator cuff syndrome is connected with the circumstances of a person’s relevant service is: (a) having an injury to the affected shoulder within the 30 days before the clinical onset of rotator cuff syndrome; or (b) performing any combination of: (i) repetitive or sustained activities of the affected shoulder when the shoulder on the affected side is abducted or flexed by a [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string rotatorCuffFactorsPara {
            get {
                return ResourceManager.GetString("rotatorCuffFactorsPara", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 
        ///
        ///
        ///    
        ///    
        ///    
        ///    
        ///    
        ///	Statement of Principles concerning rotator cuff syndrome No. 101 of 2014
        ///
        ///
        ///    Federal Register of Legislation - Australian Government
        ///    
        ///        Skip to primary navigation
        ///        Skip to primary content
        ///    
        ///        
        ///    
        ///    
        ///
        ///
        ///
        ///
        ///
        ///
        ///
        ///
        ///
        ///
        ///
        ///
        ///
        ///
        ///
        ///
        ///
        ///
        ///
        ///
        ///
        ///
        ///
        ///
        ///	
        ///	
        ///
        ///        
        ///
        ///        
        ///        
        ///
        ///
        ///
        ///        
        ///            
        ///
        ///
        ///                
        ///                    
        ///                        
        ///                            
        ///                            
        ///                                
        ///               [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string rotatorCuffText {
            get {
                return ResourceManager.GetString("rotatorCuffText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 
        ///
        ///&lt;!DOCTYPE html PUBLIC &quot;-//W3C//DTD XHTML 1.0 Transitional//EN&quot; &quot;http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd&quot;&gt;
        ///&lt;html xmlns=&quot;http://www.w3.org/1999/xhtml&quot;&gt;
        ///&lt;head id=&quot;Head1&quot;&gt;&lt;meta name=&quot;viewport&quot; content=&quot;width=device-width,initial-scale=1, user-scalable=yes&quot; /&gt;
        ///    &lt;script src=&quot;/Scripts/jquery-1.3.2.min.js&quot;&gt;&lt;/script&gt;
        ///    &lt;meta http-equiv=&quot;Content-Type&quot; content=&quot;text/html; charset=UTF-8&quot; /&gt;&lt;link id=&quot;linkCSS&quot; type=&quot;text/css&quot; rel=&quot;stylesheet&quot; href=&quot;/Content/Site.css&quot; /&gt;&lt;link type=&quot;text/css&quot; r [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string Statement_of_Principles_concerning_lumbar_spondylosis_No__62_of_2014 {
            get {
                return ResourceManager.GetString("Statement_of_Principles_concerning_lumbar_spondylosis_No__62_of_2014", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 
        ///
        ///&lt;!DOCTYPE html PUBLIC &quot;-//W3C//DTD XHTML 1.0 Transitional//EN&quot; &quot;http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd&quot;&gt;
        ///&lt;html xmlns=&quot;http://www.w3.org/1999/xhtml&quot;&gt;
        ///&lt;head id=&quot;Head1&quot;&gt;&lt;meta name=&quot;viewport&quot; content=&quot;width=device-width,initial-scale=1, user-scalable=yes&quot; /&gt;
        ///    &lt;script src=&quot;/Scripts/jquery-1.3.2.min.js&quot;&gt;&lt;/script&gt;
        ///    &lt;meta http-equiv=&quot;Content-Type&quot; content=&quot;text/html; charset=UTF-8&quot; /&gt;&lt;link id=&quot;linkCSS&quot; type=&quot;text/css&quot; rel=&quot;stylesheet&quot; href=&quot;/Content/Site.css&quot; /&gt;&lt;link type=&quot;text/css&quot; rel=&quot;S [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string Statement_of_Principles_concerning_rotator_cuff_syndrome_No__101_of_2014 {
            get {
                return ResourceManager.GetString("Statement_of_Principles_concerning_rotator_cuff_syndrome_No__101_of_2014", resourceCulture);
            }
        }
    }
}
