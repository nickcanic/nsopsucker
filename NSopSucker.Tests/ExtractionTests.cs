﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NSopSucker.Tests
{
    [TestClass]
    public class ExtractionTests
    {
        [TestMethod]
        public void ExtractTextFromHtml()
        {

            var testHtml = Properties.Resources.Statement_of_Principles_concerning_lumbar_spondylosis_No__62_of_2014;
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(testHtml);
            var text = NSopSucker.SopTextExtraction.GetText(doc);
            Console.WriteLine(text);
            Assert.IsTrue(text.Contains("Statement of Principles concerning lumbar spondylosis No. 62 of 2014"));

        }

        [TestMethod]
        public void ExtractTextFromHtmlForRotatorCuff()
        {

            var testHtml = Properties.Resources.Statement_of_Principles_concerning_rotator_cuff_syndrome_No__101_of_2014;
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(testHtml);
            var text = NSopSucker.SopTextExtraction.GetText(doc);
            Console.WriteLine(text);
            Assert.IsTrue(text.Contains("Statement of Principles concerning rotator cuff syndrome No. 101 of 2014"));

        }


        [TestMethod]
        public void LoadRefs()
        {
            var lines = Properties.Resources.fRefs;
            var rRefs = NSopSucker.SopTextExtraction.LoadFRefs(lines);
            Assert.IsTrue(rRefs.Any() && rRefs.All(r => Regex.IsMatch(r, @"F[A-Z0-9]+")));

        }

        [TestMethod]
        public void ExtractFactorsPara()
        {
            var test = Properties.Resources.lumbarSpondylosisText;
            var r = SopTextExtraction.ExtractPara(test, "Factors");
            Console.WriteLine(r);
            Assert.IsTrue(r.StartsWith("Factors") && r.EndsWith("spondylosis."));
        }


        [TestMethod]
        public void ExtractFactorsParaFromRotatorCuff()
        {
            var test = Properties.Resources.rotatorCuffText;
            var r = SopTextExtraction.ExtractPara(test, "Factors");
            Console.WriteLine(r);
            Assert.IsTrue(r.StartsWith("Factors") && r.EndsWith("syndrome."));
        }



        [TestMethod]
        public void ExtractDefinitionsPara()
        {
            var test = Properties.Resources.lumbarSpondylosisText;
            var r = SopTextExtraction.ExtractPara(test, "Other definitions");
            Console.WriteLine(r);
        }





    }
}
