﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NSopSucker.Tests
{
    [TestClass]
    public class ScraperTests
    {

        [TestMethod]
        public void Scrape()
        {

            var ids = Properties.Resources.fRefs.Split(new[] { '\n' }).Select(i => i.Trim()).Take(1);

            DirectoryInfo outputDir = new DirectoryInfo(@"C:\Users\nick_000\Desktop\ScrapedSops");
            foreach (var id in ids)
            {
                var htmlDoc = WebScraper.FetchSopHtmlDocument(id);
                Console.WriteLine(htmlDoc.DocumentNode.SelectSingleNode("//head/title").InnerText);
                htmlDoc.Save(Path.Combine(outputDir.FullName, $"{id}.html"));
            }

        }
    }
}
