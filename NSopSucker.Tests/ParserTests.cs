﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSopSucker.Model;
using Sprache;
using static NSopSucker.Parsers.SopTextParsers;

namespace NSopSucker.Tests
{
    [TestClass]
    public class ParserTests
    {
        [TestMethod]
        public void TestSectionHeading()
        {
            var testText = Properties.Resources.FactorsPara;
            var result = sectionHeading.Parse(testText);
            Assert.IsTrue(result.Number == 6 && result.Text == "Factors");
        }

        [TestMethod]
        public void TestHead()
        {
            var testText =
                @"         The factor that must as a minimum exist before it can be said that a reasonable hypothesis has been raised connecting lumbar spondylosis or death from lumbar spondylosis with the circumstances of a person’s relevant service is:";

            var result = head.Parse(testText);
            Assert.IsTrue(result == @"The factor that must as a minimum exist before it can be said that a reasonable hypothesis has been raised connecting lumbar spondylosis or death from lumbar spondylosis with the circumstances of a person’s relevant service is");
        }

        [TestMethod]
        public void TestPara()
        {
            var input =
                @"(a) being a prisoner of war before the clinical onset of lumbar spondylosis; or ";

            var result = simplePara.Parse(input);
            Assert.IsTrue(result.Reference == "a" && result.Text == "being a prisoner of war before the clinical onset of lumbar spondylosis");

        }

        [TestMethod]
        public void TestMultipleParas()
        {
            var input =
                @"(z) carrying loads of at least 25 kilograms while bearing weight through the lumbar spine to a cumulative total of at least 3 800 hours within any ten year period before the clinical worsening of lumbar spondylosis; or (aa) being obese for at least ten years before the clinical worsening of lumbar spondylosis; or (bb) flying in a powered aircraft as operational aircrew, for a cumulative total of at least 1 000 hours within the 25 years before the clinical worsening of lumbar spondylosis; or (cc) extreme forward flexion of the lumbar spine for a cumulative total of at least 1 500 hours before the clinical worsening of lumbar spondylosis; or (dd) having acromegaly involving the lumbar spine before the clinical worsening of lumbar spondylosis; or (ee) having Paget's disease of bone involving the lumbar spine before the clinical worsening of lumbar spondylosis; or (ff) inability to obtain appropriate clinical management for lumbar spondylosis.";

            var result = simplePara.Many().Parse(input);
            Assert.IsTrue(result.Count() == 7);

        }

        [TestMethod]

        public void TestParaTerminator()
        {
            var input = ".";
            var result = paraTerminator.Parse(input);
            Assert.IsTrue(result == ".");
        }

        [TestMethod]

        public void TestParaTerminator2()
        {
            var input = "; or ";
            var result = paraTerminator.Parse(input);
            Assert.IsTrue(result == "; or ");
        }
        [TestMethod]
        [ExpectedException(typeof(ParseException))]
        public void TestParaTerminator3()
        {
            var input = "something";
            var result = paraTerminator.Parse(input);

        }



        [TestMethod]
        public void TestSubParas()
        {
            var input =
                @"(l) performing any combination of: (i) repetitive or sustained activities of the affected shoulder when the shoulder on the affected side is abducted or flexed by at least 60 degrees; or (ii) forceful activities with the affected upper limb; for at least 160 hours within a period of 210 consecutive days before the clinical worsening of rotator cuff syndrome, and where the repetitive or sustained or forceful activities have not ceased more than 30 days before the clinical worsening of rotator cuff syndrome; or ";

            var result = paraWithSubParas.Parse(input);
            Assert.IsTrue(result.Reference == "l" && result.SubParas.Count() == 2);

            Console.WriteLine(result);
        }


        [TestMethod]
        public void TestWholeSection()
        {
            var input = Properties.Resources.rotatorCuffFactorsPara;
            var result = section.Parse(input);
            Assert.IsTrue(result.Paragraphs.Count() == 21);
            Assert.IsTrue(result.GetStandardOfProof() == StandardOfProof.BoP);
            foreach (var para in result.Paragraphs)
            {
                Console.WriteLine(para);
            }

        }


        [TestMethod]
        public void TestParseSingleDefinition()
        {
            var input = "\"a depositional joint disease\" means gout, calcium pyrophosphate dihydrate deposition disease (also known as pseudogout), haemochromatosis, Wilson’s disease or alkaptonuria (also known as ochronosis); \"akyphotic abnormality";

            var result = definition.Parse(input);
            Assert.IsTrue(result.DefinedWordOrPhrase == "a depositional joint disease" &&
                result.Meaning == "means gout, calcium pyrophosphate dihydrate deposition disease (also known as pseudogout), haemochromatosis, Wilson’s disease or alkaptonuria (also known as ochronosis)");


        }


    }
}
